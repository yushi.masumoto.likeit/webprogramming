package util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

public class PasswordEncorder {

  public static String encordPassword(String password) {



    String str = password;

    Charset charset = StandardCharsets.UTF_8;

    String algorithm = "MD5";

    String encodeStr = null;

    try {
      byte[] bytes = MessageDigest.getInstance(algorithm).digest(str.getBytes(charset));

      encodeStr = DatatypeConverter.printHexBinary(bytes);

    } catch (NoSuchAlgorithmException e) {

      e.printStackTrace();
    }
    return encodeStr;

  }

}
