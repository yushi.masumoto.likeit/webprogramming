package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAdd
 */
@WebServlet("/UserAdd")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.setCharacterEncoding("UTF-8");

        HttpSession session = request.getSession();
        User userInfo = (User) session.getAttribute("userInfo");

        if (userInfo == null) {

          response.sendRedirect("LoginServlet");
        return;
      }

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.setCharacterEncoding("UTF-8");

        String loginId = request.getParameter("user-loginid");
        String password = request.getParameter("password");
        String password_confirm = request.getParameter("password-confirm");
        String name = request.getParameter("user-name");
        String birthday = request.getParameter("birth-date");
        


        UserDao userDao = new UserDao();
        User user = userDao.findByLoginInfo(loginId, password);


        if ((loginId.equals("") || password.equals("") || password_confirm.equals("")
            || name.equals("") || birthday.equals("")) || !(password.equals(password_confirm))
            || user != null) {
          request.setAttribute("errMsg", "入力された値は正しくありません");

          request.setAttribute("inputLoginId", loginId);
          request.setAttribute("inputName", name);
          request.setAttribute("inputBirthday", birthday);

          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
          dispatcher.forward(request, response);

          return;

        } else {

          String pass = PasswordEncorder.encordPassword(password);

          UserDao.insert(loginId, pass, name, birthday);


        

        }

        response.sendRedirect("UserListServlet");

	}

}
