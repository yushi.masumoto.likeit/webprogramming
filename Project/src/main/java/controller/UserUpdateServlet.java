package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserUpdateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();
    User userInfo = (User) session.getAttribute("userInfo");

    if (userInfo == null) {

      response.sendRedirect("LoginServlet");
      return;
    }

    String strId = request.getParameter("id");
    int id = Integer.valueOf(strId);

    UserDao userDao = new UserDao();
    User user = userDao.findById(id);

    request.setAttribute("user", user);

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    request.setCharacterEncoding("UTF-8");

    String user_Id = request.getParameter("user-id");
    String password = request.getParameter("password");
    String password_Confirm = request.getParameter("password-confirm");
    String user_Name = request.getParameter("user-name");
    String birth_Date = request.getParameter("birth-date");



    UserDao userDao = new UserDao();
    User user = userDao.findByLoginInfo(user_Id, password);


    if ((user_Id.equals("") || password.equals("") || password_Confirm.equals("")
        || user_Name.equals("") || birth_Date.equals("")) || !(password.equals(password_Confirm))
        || user != null) {
      request.setAttribute("errMsg", "入力された値は正しくありません");

      request.setAttribute("inputLoginId", user_Id);
      request.setAttribute("inputName", user_Name);
      request.setAttribute("inputBirthday", birth_Date);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);

      return;
    }

    String pass = PasswordEncorder.encordPassword(password);
    

    String id = request.getParameter("id");
    if (password.equals("") && password_Confirm.equals("")) {
      UserDao.insert2(user_Id, user_Name, birth_Date, id);
    } else {
      UserDao.insert1(user_Id, pass, user_Name, birth_Date, id);
    }
    response.sendRedirect("UserListServlet");
  }

}
