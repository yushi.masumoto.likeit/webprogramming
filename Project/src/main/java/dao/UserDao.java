package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

public class UserDao {

  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // TODO: 未実装：管理者以外を取得するようSQLを変更する
      String sql = "SELECT * FROM user WHERE is_admin = false";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  public static void insert(String loginId, String password, String name, String birthday) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();



      String sql =
          "INSERT INTO user (login_id, password, name, birth_date, create_date, update_date)  VALUES (?, ?, ?, ?, now(),now())";
      PreparedStatement pSmt = conn.prepareStatement(sql);

      pSmt.setString(1, loginId);
      pSmt.setString(2, password);
      pSmt.setString(3, name);
      pSmt.setString(4, birthday);

      pSmt.executeUpdate();





    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
      }
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }

  }

  public static User findById(int id) {
    Connection conn = null;
    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE id = ? ";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id1 = rs.getInt("id");
      String _loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id1, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }



  }

  public static void insert1(String loginId, String password, String name, String birthday,
      String id) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql =
          "UPDATE user SET login_id = ?, password = ?, name = ?, birth_date = ? WHERE id = ?";

      PreparedStatement pSmt = conn.prepareStatement(sql);
      pSmt.setString(1, loginId);
      pSmt.setString(2, password);
      pSmt.setString(3, name);
      pSmt.setString(4, birthday);
      pSmt.setString(5, id);

      pSmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
      }
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }


  }

  public static void insert2(String loginId, String name, String birthday, String id) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "UPDATE user SET login_id = ?, name = ?, birth_date = ? WHERE id = ?";

      PreparedStatement pSmt = conn.prepareStatement(sql);
      pSmt.setString(1, loginId);
      pSmt.setString(2, name);
      pSmt.setString(3, birthday);
      pSmt.setString(4, id);

      pSmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
      }
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }


  }


  public static void delete(String id) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "DELETE FROM user WHERE id = ?";

      PreparedStatement pSmt = conn.prepareStatement(sql);

      pSmt.setString(1, id);

      pSmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
      }
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }

  }


  public static List<User> search(String loginId, String userName, String startBirthDate,
      String endBirthDate) {

    Connection conn = null;
    List<User> userList = new ArrayList<User>();
    try {

      conn = DBManager.getConnection();



      /*
       * String sql = "SELECT * FROM user WHERE is_admin = false"; if (loginId != null &&
       * !loginId.equals("")) { sql += " AND login_id = ?"; } if (userName != null &&
       * !userName.equals("")) { sql += " AND name LIKE ?"; } if (startBirthDate != null &&
       * !startBirthDate.equals("")) { sql += " AND ? <= birth_date"; } if (endBirthDate != null &&
       * !endBirthDate.equals("")) { sql += " AND birth_date <= ?"; }
       */

      String str = "SELECT * FROM user WHERE is_admin = false";
      String str1 = " AND login_id = ?";
      String str2 = " AND name LIKE ?";
      String str3 = " AND ? <= birth_date";
      String str4 = " AND birth_date <= ?";

      StringBuilder stringBuilder = new StringBuilder(str);

      if (loginId != null && !loginId.equals("")) {
        stringBuilder.append(str1);
      }
      if (userName != null && !userName.equals("")) {
        stringBuilder.append(str2);
      }
      if (startBirthDate != null && !startBirthDate.equals("")) {
        stringBuilder.append(str3);
      }
      if (endBirthDate != null && !endBirthDate.equals("")) {
        stringBuilder.append(str4);
      }
      String sql = stringBuilder.toString();
      /*
       * String sql =
       * "SELECT * FROM user WHERE login_id = ? AND name LIKE ? AND birth_date BETWEEN ? AND ? ";
       */

      PreparedStatement stmt = conn.prepareStatement(sql);

      int t = 0;

      if (loginId != null && !loginId.equals("")) {
        t = t + 1;
        stmt.setString(t, loginId);
      }
      if (userName != null && !userName.equals("")) {
        t = t + 1;
        stmt.setString(t, "%" + userName + "%");
      }
      if (startBirthDate != null && !startBirthDate.equals("")) {
        t = t + 1;
        stmt.setString(t, startBirthDate);
      }
      if (endBirthDate != null && !endBirthDate.equals("")) {
        t = t + 1;
        stmt.setString(t, endBirthDate);
      }


      /*
       * stmt.setString(1, loginId); stmt.setString(2, "%" + userName + "%"); stmt.setString(3,
       * startBirthDate); stmt.setString(4, endBirthDate);
       */

    
      ResultSet rs = stmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginIdd = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginIdd, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }
  
}
